package com.getjavajob.training.okhanzhin.birthdayreminder.dao.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories(basePackages = {"com.getjavajob.training.okhanzhin.birthdayreminder.dao"})
@EntityScan(basePackages = {"com.getjavajob.training.okhanzhin.birthdayreminder.domain"})
public class DaoConfig {
}