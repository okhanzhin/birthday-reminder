package com.getjavajob.training.okhanzhin.birthdayreminder.dao;

import com.getjavajob.training.okhanzhin.birthdayreminder.domain.Notification;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NotificationRepository extends MongoRepository<Notification, Long> {
}
