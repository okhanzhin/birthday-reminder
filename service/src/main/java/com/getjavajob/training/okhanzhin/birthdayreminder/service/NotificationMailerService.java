package com.getjavajob.training.okhanzhin.birthdayreminder.service;

import com.getjavajob.training.okhanzhin.birthdayreminder.dao.NotificationRepository;
import com.getjavajob.training.okhanzhin.birthdayreminder.domain.Notification;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class NotificationMailerService {
    private static final Logger logger = LoggerFactory.getLogger(NotificationMailerService.class);

    private final NotificationRepository notificationRepository;
    private final SequenceGeneratorService sequenceGenerator;
    private final JavaMailSender javaMailSender;

    @Autowired
    public NotificationMailerService(NotificationRepository notificationRepository,
                                     SequenceGeneratorService sequenceGenerator,
                                     JavaMailSender javaMailSender) {
        this.notificationRepository = notificationRepository;
        this.sequenceGenerator = sequenceGenerator;
        this.javaMailSender = javaMailSender;
    }

    @JmsListener(destination = "${activemq.destination}", containerFactory = "jmsFactory")
    public void receiveAndSendByMail(Notification notification) {
        notification.setNotificationID(sequenceGenerator.generateSequence(Notification.SEQUENCE_NAME));
        notificationRepository.save(notification);
        logger.info("Mail Reminder receiving > {}", notification);

        if (Objects.nonNull(notification.getRecipientEmails())) {
            SimpleMailMessage mailMessage = new SimpleMailMessage();
            mailMessage.setFrom("sketchnetwork9@gmail.com");
            mailMessage.setTo(notification.getRecipientEmails().toArray(new String[0]));
            mailMessage.setSubject("Birthday Notification");
            mailMessage.setText(notification.getContent());

            javaMailSender.send(mailMessage);
        } else {
            logger.info("No notification recipients were found.");
        }
    }
}
