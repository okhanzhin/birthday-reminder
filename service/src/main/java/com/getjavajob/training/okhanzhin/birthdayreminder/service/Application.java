package com.getjavajob.training.okhanzhin.birthdayreminder.service;

import com.getjavajob.training.okhanzhin.birthdayreminder.service.config.JmsConfig;
import com.getjavajob.training.okhanzhin.birthdayreminder.service.config.ServiceConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@EnableAutoConfiguration
@Import({ServiceConfig.class, JmsConfig.class})
@ComponentScan(basePackages = {"com.getjavajob.training.okhanzhin.birthdayreminder"})
public class Application {
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
