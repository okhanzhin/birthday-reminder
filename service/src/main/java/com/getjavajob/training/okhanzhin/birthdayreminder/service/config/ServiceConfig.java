package com.getjavajob.training.okhanzhin.birthdayreminder.service.config;

import com.getjavajob.training.okhanzhin.birthdayreminder.dao.config.DaoConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(DaoConfig.class)
public class ServiceConfig {
}
